package com.example.demo.repository;


import com.example.demo.domain.Token;
import com.example.demo.dto.GameStatusDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends JpaRepository<Token, String> {

}