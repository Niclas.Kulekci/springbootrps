package com.example.demo.repository;


import com.example.demo.domain.Game;
import com.example.demo.dto.GameStatusDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepository extends JpaRepository<Game, String> {

}