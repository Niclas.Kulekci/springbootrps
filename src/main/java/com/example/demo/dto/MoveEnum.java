package com.example.demo.dto;

public enum MoveEnum {
    NONE,
    ROCK,
    PAPER,
    SCISSORS
}
