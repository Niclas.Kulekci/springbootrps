package com.example.demo.dto;

public enum GameEnum {
    NONE,
    OPEN,
    ACTIVE,
    WIN,
    LOSE,
    DRAW
}
