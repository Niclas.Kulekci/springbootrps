package com.example.demo.dto;

import com.example.demo.domain.Game;
import com.example.demo.domain.Token;
import lombok.Value;

import java.util.Optional;

@Value
public class GameStatusDTO {
    String id;
    String name;
    String move;
    String game;
    String opponentName;
    String opponentMove;
    String gameResult;


    public static GameStatusDTO getStatus(Game game, Token token) {// might need to switch !token to token
        if (token.getOwner()) {
            Optional<Token> other = game.getTokens().stream().filter(t -> !t.getOwner()).findAny();
            if (other.isEmpty()) {
                return new GameStatusDTO(game.getId(), token.getName(), game.getMove().name(), game.getId(), null, null,game.getGameEnum().name());
            }
            return new GameStatusDTO(game.getId(), token.getName(), game.getMove().name(), game.getId(), other.get().getName(), game.getOpponentMove().name(),game.getGameEnum().name());
        } else {
            Optional<Token> other = game.getTokens().stream().filter(t -> t.getOwner()).findAny();
            if (other.isEmpty()) {
                return new GameStatusDTO(game.getId(), token.getName(), game.getMove().name(), game.getId(), null, null, game.getGameEnum().name());
            }
            //this is for else if statement so its always for opponent
            GameEnum result = game.getGameEnum();
            if (result == GameEnum.WIN) {
                result = GameEnum.LOSE;
            } else if (result == GameEnum.LOSE) {
                result = GameEnum.WIN;
            }

            return new GameStatusDTO(game.getId(), token.getName(), game.getOpponentMove().name(), game.getId(), other.get().getName(), game.getMove().name(), result.name());
        }
    }

    public static GameStatusDTO getStatus(Game game) {
        if (game.getTokens().isEmpty()) {
            return null;
        }

        Token token = game.getTokens().get(0);
        return new GameStatusDTO(game.getId(), token.getName(), game.getMove().name(), game.getId(), null, null, game.getGameEnum().name());
    }
}