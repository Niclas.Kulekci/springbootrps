package com.example.demo.services;

import com.example.demo.domain.Game;
import com.example.demo.domain.Token;
import com.example.demo.dto.GameEnum;
import com.example.demo.dto.MoveEnum;
import com.example.demo.exceptions.GameException;
import com.example.demo.dto.GameStatusDTO;
import com.example.demo.exceptions.TokenException;
import com.example.demo.repository.GameRepository;
import com.example.demo.repository.TokenRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class GameService {

    TokenRepository tokenRepository;
    GameRepository gameRepository;

    public void setName(String tokenString, String name) throws TokenException{
        Token token = tokenRepository.findById(tokenString).orElseThrow(() ->new TokenException("There is no token with that Id '" + tokenString + "'"));
        token.setName(name);
        tokenRepository.save(token);
    }

    public Game createGame(String tokenId) throws TokenException
    {
        Token token = tokenRepository.findById(tokenId).orElseThrow(() -> new TokenException("There is no token with that Id '" + tokenId + "'"));
        token.setOwner(true);
        Game game = new Game(UUID.randomUUID().toString(), List.of(token), MoveEnum.NONE, GameEnum.OPEN, MoveEnum.NONE);
        gameRepository.save(game);
        token.setGame(game);
        tokenRepository.save(token);
        return game;
    }


    public List<Game> getGames()
    {
     return gameRepository.findAll().stream().distinct()
             .filter(g ->g.getGameEnum().equals(GameEnum.OPEN))
             .collect(Collectors.toList());
    }

    public Game getGame(String gameId) throws GameException {
        return gameRepository.findById(gameId).orElseThrow(() -> new GameException("There is no game with id '" + gameId + "'"));
    }

    public GameStatusDTO join(String tokenId, String gameId) throws GameException,TokenException{
        Token token = tokenRepository.findById(tokenId).orElseThrow(() -> new TokenException("There is no token with that Id '" + tokenId + "'"));

        Game game = getGame(gameId);
        token.setGame(game);
        game.getTokens().add(token);
        game.setGameEnum(GameEnum.ACTIVE);
        tokenRepository.save(token);
        gameRepository.save(game);

        return GameStatusDTO.getStatus(game, token);
    }

    public GameStatusDTO move(String tokenId, String sign) throws TokenException{
        Token token = tokenRepository.findById(tokenId).orElseThrow(() -> new TokenException("There is no token with that Id '" + tokenId + "'"));

       // Optional<Token> pot = tokenRepository.findById(tokenId);
//        if (pot.isPresent()) {
//            pot.get().getOwner()
//        } other way of using orElseThrow();

        MoveEnum moveEnum;
        try {
            moveEnum = MoveEnum.valueOf(sign);
        } catch (IllegalArgumentException e) {
            return null;
        }

        Game game = token.getGame();
        if (token.getOwner()){
            game.setMove(moveEnum);

            if (game.getOpponentMove() != MoveEnum.NONE) {
               game.setGameEnum(CalculateWinner.calculateWinner(game));
            }
        } else {
            game.setOpponentMove(moveEnum);

            if (game.getMove() != MoveEnum.NONE) {
                game.setGameEnum(CalculateWinner.calculateWinner(game));
            }
        }

        gameRepository.save(game);
        return GameStatusDTO.getStatus(game, token);
    }




}
