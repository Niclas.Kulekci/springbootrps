package com.example.demo.services;

import com.example.demo.domain.Game;
import com.example.demo.dto.GameEnum;
import com.example.demo.dto.MoveEnum;

import static com.example.demo.dto.MoveEnum.*;

public class CalculateWinner {

    public static GameEnum calculateWinner(Game game){
        MoveEnum move = game.getMove();
        MoveEnum opponentMove = game.getOpponentMove();

        if(move.equals(opponentMove))
            return GameEnum.DRAW;
        switch(move){
            case PAPER: return opponentMove.equals(MoveEnum.SCISSORS) ? GameEnum.LOSE : GameEnum.WIN;
            case SCISSORS: return opponentMove.equals(MoveEnum.ROCK) ? GameEnum.LOSE : GameEnum.WIN;
            case ROCK: return opponentMove.equals(MoveEnum.PAPER) ? GameEnum.LOSE : GameEnum.WIN;
            default: throw new RuntimeException("Bad move" + move);
        }
    }

}
