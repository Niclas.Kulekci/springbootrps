package com.example.demo.services;

import com.example.demo.domain.Token;
import com.example.demo.repository.TokenRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class TokenService {

    TokenRepository tokenRepository;

    public Token createNewToken()
    {
        Token token = new Token(UUID.randomUUID().toString());
        tokenRepository.save(token);
        return token;
    }

    public Token getToken(String tokenString){
        return tokenRepository.findById(tokenString).get();
    }


    public Token getOrCreateToken(String tokenString)
    {
        Optional<Token> byId = tokenRepository.findById(tokenString);
        if(byId.isPresent())
        {
            return tokenRepository.findById(tokenString).get();
        }
        return createNewToken();
    }

}
