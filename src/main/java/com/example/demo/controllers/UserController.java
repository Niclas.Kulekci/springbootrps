package com.example.demo.controllers;

import com.example.demo.services.GameService;
import com.example.demo.services.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:19006")
@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    TokenService tokenService;
    GameService gameService;

    @PostMapping("/name")
    public void setName(@RequestBody String userName,
                          @RequestHeader(value = "token", required = true) String tokenString) throws Exception{
      gameService.setName(tokenString, userName);
    }
}


