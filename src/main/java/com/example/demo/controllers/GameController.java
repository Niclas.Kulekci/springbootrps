package com.example.demo.controllers;

import com.example.demo.domain.Game;
import com.example.demo.domain.Token;
import com.example.demo.dto.GameStatusDTO;
import com.example.demo.exceptions.TokenException;
import com.example.demo.services.GameService;
import com.example.demo.services.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@CrossOrigin(origins = "http://localhost:19006")
@RestController
@RequestMapping("/games")
@AllArgsConstructor
public class GameController {

    TokenService tokenService;
    GameService gameService;


    @GetMapping("/start")
    public GameStatusDTO start(@RequestHeader(value = "token") String tokenId) throws Exception
    {
        Game game = gameService.createGame(tokenId);
        Token token = tokenService.getToken(tokenId);
        return GameStatusDTO.getStatus(game, token);
    }

//    private GameStatusDTO toGameStatus(String tokenId, Game game)
//    {
//        Token token1 = game.getTokens().get(0);
//        Token token2 = game.getTokens().get(1);
//        if (token1.getId().equals(tokenId))
//        {
//            // Första spelaren
//            return new GameStatusDTO(game.getId(), token1.getName(), game.getMove().name(), game.getId(), token2 != null ? token2.getName() : null,game.getOpponentMove().name());
//        } else {
//            // Andra spelaren
//            return new GameStatusDTO(game.getId(), token2.getName(), game.getOpponentMove().name(), game.getId(), token1.getName(), game.getMove().name());
//        }
//    }

    @GetMapping("")
    public List<GameStatusDTO> all (@RequestHeader("token")String tokenId)
    {
        List<GameStatusDTO> list = new ArrayList<>();
        for (Game game : gameService.getGames())
            list.add(GameStatusDTO.getStatus(game));

        return list;
    }

    @GetMapping("/status")
    public GameStatusDTO getStatus(@RequestHeader(value = "token") String tokenId)
    {
        Token token = tokenService.getOrCreateToken(tokenId);
        Game game = token.getGame();
        return GameStatusDTO.getStatus(game, token);
    }

    @GetMapping("/join/{gameId}")
    public GameStatusDTO joinGame(@RequestHeader("token") String tokenId, @PathVariable("gameId") String gameId) throws Exception
    {
        return gameService.join(tokenId, gameId);
//        Token token = tokenService.getOrCreateToken(tokenId);
//        Game game = gameService.getGame(gameId);
//        return GameStatusDTO.getStatus(token);
    }
    @GetMapping("/{gameId}")
    public GameStatusDTO getGame(@RequestHeader("token")String tokenId,@PathVariable("gameId") String gameId) throws Exception
    {
        Token token = tokenService.getOrCreateToken(tokenId);
        Game game = gameService.getGame(gameId);
        return GameStatusDTO.getStatus(game,token);
    }

    @GetMapping("/move/{sign}")
    public GameStatusDTO move(@RequestHeader("token")String tokenId, @PathVariable("sign") String sign) throws Exception{
        return gameService.move(tokenId, sign);
    }
}

