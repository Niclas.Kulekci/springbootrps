package com.example.demo.controllers;

import com.example.demo.domain.Token;
import com.example.demo.services.TokenService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:19006")
@RestController
@RequestMapping ("/auth")
@AllArgsConstructor
public class TokenController {

    TokenService tokenService;

    @GetMapping("/token")
    public String createNewToken()
    {
       return tokenService.createNewToken().getId();
    }

}
