package com.example.demo.domain;

import com.example.demo.dto.GameEnum;
import com.example.demo.dto.GameStatusDTO;
import com.example.demo.dto.MoveEnum;
import com.example.demo.repository.TokenRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Game {

    @Id String id;

    @OneToMany
    List<Token> tokens;

    @Enumerated(EnumType.STRING)
    MoveEnum move = MoveEnum.NONE;
    @Enumerated(EnumType.STRING)
    GameEnum gameEnum = GameEnum.NONE;
    @Enumerated(EnumType.STRING)
    MoveEnum opponentMove = MoveEnum.NONE;

    /* Database Visualised
    Game:
    id move gameEnum opponentMove
    1  ROCK OPEN PAPER
    2  ROCK OPEN PAPER

    Token:
    id name gameId
    1  Niclas 1
    2  Arne   1
    3  Oscar  2
    4  Kajsa  2


     */
}

