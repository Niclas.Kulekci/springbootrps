package com.example.demo.exceptions;

import com.example.demo.domain.Game;
import com.example.demo.domain.Token;
import javassist.tools.rmi.ObjectNotFoundException;

import java.util.Collection;

public class TokenException extends Exception{

    public TokenException(String message) {
        super(message);
    }

//    Token findTokenException(Collection<Token> tokens, String tokenId) throws ObjectNotFoundException {
//        for(Token token : tokens){
//            if(token.getId().equals(tokenId)){
//                return token;
//            }
//        }
//        throw new ObjectNotFoundException("message here");
//    }
}


