package com.example.demo.exceptions;

public class GameException extends Exception {

    public GameException(String message) {
        super(message);
    }

//    Game GameException(Collection<Game> game, String gameId) throws ObjectNotFoundException {
//        for(Game id : game){
//            if(id.getId().equals(gameId)){
//                return id;
//            }
//        }
//        throw new ObjectNotFoundException("Game with id '" + gameId + "' not found.");
//    }
}
