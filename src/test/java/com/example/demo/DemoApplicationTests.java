package com.example.demo;

import com.example.demo.domain.Game;
import com.example.demo.domain.Token;
import com.example.demo.dto.GameEnum;
import com.example.demo.dto.MoveEnum;
import com.example.demo.repository.GameRepository;
import com.example.demo.repository.TokenRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integrationtest")
class DemoApplicationTests {

    @LocalServerPort private int port;
    private TestRestTemplate testRestTemplate;

    @Autowired
    TokenRepository tokenRepository;
    GameRepository gameRepository;

    Token[] Token;

    @BeforeEach
    void setup(){
        testRestTemplate = new TestRestTemplate();

    }

    @Test
    void test_get_new_token() {

        //Given

        //When
        String token = testRestTemplate.getForObject(testUrl("/auth/token"),String.class);
        System.out.println("DemoTestPrintln"+token);

        //Then
        assertNotNull(token);
    }


    @Test
    void test_set_name() {

        //Given
        tokenRepository.save(new Token(UUID.randomUUID().toString(),"Niclas",null,false));
        //When
        String setName = testRestTemplate.getForObject(testUrl("/user/name"),String.class);
        System.out.println("DemoTestPrintln"+setName);

        //Then
        assertNotNull(setName);

    }
    @Test
    void test_start_games() {

        //Given
        gameRepository.save(new Game(UUID.randomUUID().toString(), List.of(Token), MoveEnum.NONE, GameEnum.OPEN,MoveEnum.NONE));
        tokenRepository.save(new Token(UUID.randomUUID().toString(),"Niclas", (Game) gameRepository, true));
        //When
        String startGame = testRestTemplate.getForObject(testUrl("/games/start"),String.class);
        System.out.println("DemoTestPrintln"+startGame);

        //Then
        assertNotNull(startGame);

    }
    private String testUrl(String path){
        return "http://localhost:"+port+path;
    }

}
